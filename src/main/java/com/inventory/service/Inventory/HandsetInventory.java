package com.inventory.service.Inventory;

import com.inventory.models.handset.MobileHandset;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface HandsetInventory {

/*
    Load handset information
        - handset
        - fono API
        - set default/init value for inventory item
*/
    void initService() throws IOException, ParseException, URISyntaxException, InterruptedException;

    /*
    Service methods
        - handset
        - fono API
        - set default/init value for inventory item
*/
    List<MobileHandset> getBooked();

    List<MobileHandset> getBooked(String brand);

    List<MobileHandset> getAvailable();

    List<MobileHandset> getAvailable(String brand);

    void borrow(String imei, String borrower);
    void putBack(String imei);

}
