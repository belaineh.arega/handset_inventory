package com.inventory.service.Inventory;

import com.inventory.models.handset.HandsetTechSpec;
import com.inventory.models.handset.MobileHandset;
import com.inventory.service.proxy.FonoClient;
import lombok.NoArgsConstructor;
import org.json.simple.*;
import org.json.simple.parser.*;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.inventory.service.proxy.FonoClient.getTechnology;
import static com.inventory.service.proxy.FonoClient.token;
import static java.lang.System.exit;

@NoArgsConstructor
public class HandsetInventoryImp implements HandsetInventory {

    // inventory data
    public static List<MobileHandset> mobileHandsets = new ArrayList<>();
    private boolean fonoApiStatus;

    @Override
    public void initService() throws IOException, ParseException, URISyntaxException, InterruptedException {
        initFono();
         // logger could be used
        // parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("./src/main/resources/handset.json"))
        {
            System.out.println("\n ##############> Starting Handset inventory ...  ##############");
            Object handsetEntity = jsonParser.parse(reader);
            JSONArray handsetList = (JSONArray) handsetEntity;
            //Iterate over Handset array
            handsetList.forEach( handset -> {
                try {
                    loadInventory( (JSONObject) handset );
                } catch (URISyntaxException | InterruptedException | IOException e) {
                    throw new RuntimeException(e);
                }
            });
            System.out.println("\n ##############> Started ##############");
        }
    }

    @Override
    public List<MobileHandset> getBooked() {
        // this could be logger
        System.out.println("\n ##############> List of All booked Handsets ############## \n");

        return mobileHandsets
                .stream()
                .filter(handset -> !handset.isAvailability())
                .collect(Collectors.toList());

    }
    @Override
    public List<MobileHandset> getBooked(String brand) {
        // this could be logger
        System.out.println("\n ##############> List of booked Handsets by Brand ############## \n");

        return mobileHandsets
                .stream()
                .filter(handset -> (handset.isAvailability()== false) && handset.getBrand().equals(brand))
                .collect(Collectors.toList());

    }

    @Override
    public List<MobileHandset> getAvailable() {
        // this could be logger
        System.out.println("\n ##############> List of All Available Handsets ############## \n");

        return mobileHandsets
                .stream()
                .filter(handset -> handset.isAvailability())
                .collect(Collectors.toList());

    }
    @Override
    public List<MobileHandset> getAvailable(String brand) {
        // this could be logger
        System.out.println("\n ##############> List of Available Handsets by Brand ############## \n");

        return mobileHandsets
                .stream()
                .filter(handset -> (handset.isAvailability() == true) && handset.getBrand().equals(brand))
                .collect(Collectors.toList());

    }

    @Override
    public void borrow(String imei, String borrower) {

        try {
            mobileHandsets.forEach((handset) -> {
                if (handset.getImie().equals(imei)) {
                    handset.setBorrower(MobileHandset.team.valueOf(borrower));
                    handset.setBorrowingTime(Timestamp.from(ZonedDateTime.now().toInstant()));
                    handset.setAvailability(false);
                }
            });
        }catch (IllegalArgumentException e){
            System.out.println("Borrower should be QA team");
        }

    }
    @Override
    public void putBack (String imei) {

        mobileHandsets.forEach((handset) -> {
            if(handset.getImie().equals(imei)){
                handset.setBorrower(null);
                handset.setAvailability(true);
            }
        });

    }

    private void loadInventory(JSONObject Handset) throws URISyntaxException, IOException, InterruptedException {

        HandsetTechSpec spec = null;
        List<Boolean> band;
        boolean twog, threeg, fourg;
        String twogTmp, threegTmp, fourgTmp;
        String brand,imei, model;

        //Get Handset object within list
        JSONObject HandsetEntities = (JSONObject) Handset.get("phone");

        // load spec from fono
        if(fonoApiStatus){
            band = getTechnology((String) HandsetEntities.get("model"), token);
            if(!band.isEmpty()) spec = new HandsetTechSpec(band.get(0),band.get(1),band.get(2));

        }else {
            // fall back   // load from file
            twogTmp= (String)((JSONObject)HandsetEntities.get("default_spec")).get("twoG");
            threegTmp=(String)((JSONObject)HandsetEntities.get("default_spec")).get("threeG");
            fourgTmp=(String)((JSONObject)HandsetEntities.get("default_spec")).get("fourG");

            twog = twogTmp.equals("enabled") ;
            threeg = threegTmp.equals("enabled") ;
            fourg = fourgTmp.equals("enabled");
            spec = new HandsetTechSpec(twog,threeg,fourg);

        }
        // load MobileHandset info
            imei = (String)HandsetEntities.get("imei");
            brand = (String)HandsetEntities.get("brand");
            model = (String)HandsetEntities.get("model");

            mobileHandsets.add(new MobileHandset(imei,brand,model,true,null,spec,null));

    }
    private void initFono() throws URISyntaxException, IOException, InterruptedException {

        if(!FonoClient.generateToken().equals("null")){
            fonoApiStatus = true;
        }
    }

}
