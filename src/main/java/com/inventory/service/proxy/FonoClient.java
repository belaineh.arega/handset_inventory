package com.inventory.service.proxy;

import lombok.NoArgsConstructor;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class FonoClient {
    public static String token;
    private static HttpClient client = HttpClient.newHttpClient();
    private static HttpRequest request ;

    public static String generateToken() throws URISyntaxException, IOException, InterruptedException {

        token="null";
        boolean heartBitStatus = false;

        request = HttpRequest.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .uri(URI.create("https://fonoapi.freshpixl.com/token/generate"))
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

        if(response.statusCode() == 200){
            heartBitStatus = true;
            token = response.body();
        }

        return token;
    }

    public static List<Boolean> getTechnology(String device, String token) throws URISyntaxException, IOException, InterruptedException {

       List<Boolean> bandWidth = new ArrayList<>(3);

        request = HttpRequest.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .uri(URI.create("https://fonoapi.freshpixl.com/token=token&device=device"))
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

        if(response.statusCode() == 200){
            // TODO
            //parsing the bandwidth would continue here
            // but Fono API have never worked
        }
        return bandWidth;
    }
}