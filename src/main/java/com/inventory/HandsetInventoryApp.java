package com.inventory;

import com.inventory.models.handset.HandsetTechSpec;
import com.inventory.models.handset.MobileHandset;
import com.inventory.service.Inventory.HandsetInventory;
import com.inventory.service.Inventory.HandsetInventoryImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HandsetInventoryApp {
    private static HandsetInventory handsetInventory = new HandsetInventoryImp();

    public static void main(String[] args) throws Exception {

        handsetInventory.initService();
        processRequest();

    }
    public static int serviceMenu(){

        System.out.println("\n ##############> Choose one of the Services ##############");

        System.out.println("\n <#######> get list of available handsets ----------pres 1");
        System.out.println("\n <#######> get list of available handsets by brand -pres 2");

        System.out.println("\n <##########> get list of Booked handsets ----------pres 3");
        System.out.println("\n <##########> get list of Booked handsets by brand -pres 4");

        System.out.println("\n <################> Book handset  ----------------- pres 5");
        System.out.println("\n <################> Return handset  --------------- pres 6");

        System.out.println("\n <################> Exit--------------------------- pres 0");

        Scanner mainMenu = new Scanner(System.in);
        return mainMenu.nextInt();

    }

    // this would have been an API end point :-)
    public static void processRequest(){
        // begin
        int userInput = 9;
        String brand, imei , teamMember;
        HandsetTechSpec spec;
        Scanner subMenu = null;
        List<MobileHandset> result = new ArrayList<>();

        do {
            userInput = serviceMenu();

            switch (userInput) {

                case 1:
                    printMobileHandset(handsetInventory.getAvailable());
                    break;
                case 2:
                    System.out.println("############ Enter Brand ###");
                    subMenu = new Scanner(System.in);
                    brand = subMenu.nextLine();
                    printMobileHandset(handsetInventory.getAvailable(brand));
                    break;
                case 3:
                    printMobileHandset(handsetInventory.getBooked());
                    break;
                case 4:
                    System.out.println("############ Enter Brand ###");
                    subMenu = new Scanner(System.in);
                    brand = subMenu.nextLine();
                    printMobileHandset(handsetInventory.getBooked(brand));
                    break;
                case 5:
                    System.out.println("############ Enter IMEI ###");
                    subMenu = new Scanner(System.in);
                    imei = subMenu.nextLine();
                    System.out.println("############ Enter borower ###");
                    subMenu = new Scanner(System.in);
                    teamMember = subMenu.nextLine();
                    handsetInventory.borrow(imei, teamMember);
                    break;
                case 6:
                    System.out.println("############ Enter IMEI ###");
                    subMenu = new Scanner(System.in);
                    imei = subMenu.nextLine();
                    handsetInventory.putBack(imei);
                    break;
                default:
                    break;
            }

        }while (userInput != 0);
        subMenu.close();
    }

    public static void printMobileHandset(List<MobileHandset> result){

        result.forEach((handset) -> {
            System.out.println( "\n IMEI *** "+ handset.getImie());
            System.out.println( "\n Brand *** "+ handset.getBrand());
            System.out.println( "\n Model *** "+ handset.getModel());
            System.out.println( "\n Borrowed time ***"+ handset.getBorrowingTime());
            System.out.println( "\n Availability *** "+ handset.isAvailability());
            System.out.println( "\n Borrower *** "+ handset.getBorrower());
            System.out.println( "\n TwoG capable *** "+ handset.getSpec().isTwoG());
            System.out.println( "\n ThreeG capable  *** "+ handset.getSpec().isThreeG());
            System.out.println( "\n FourG capable  *** "+ handset.getSpec().isFourG());
        });
    }
}
