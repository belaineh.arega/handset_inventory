package com.inventory.models.handset;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
//import main.java.models.proxy.BandwidthSupport;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandsetTechSpec {

    private boolean twoG;
    private boolean threeG;
    private boolean fourG;

}
