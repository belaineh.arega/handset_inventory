package com.inventory.models.handset;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MobileHandset {
    // id
    private String imie;
    private String brand;
    private String model;
    // live data
    private boolean availability;
    private Timestamp borrowingTime;
    // spec
    private HandsetTechSpec spec;
    // ENSURING ONLY TEAM QA IS COULD BORROW
    // THIS COULD HAVE BEEN independent  ENTITY BUT
    // IT HAS ONLY ONE ROLE
    public static enum team {LEE, LISA, AHMED}

    private team borrower;


}
